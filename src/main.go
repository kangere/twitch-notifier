package main

import (
	"bytes"
	"crypto/hmac"
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"time"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/secretsmanager"
	"github.com/aws/aws-sdk-go/service/sns"
)

var (
	// DefaultHTTPGetAddress Default Address
	DefaultHTTPGetAddress = "https://checkip.amazonaws.com"

	// ErrNoIP No IP found in response
	ErrNoIP = errors.New("No IP in HTTP response")

	// ErrNon200Response non 200 status code in response
	ErrNon200Response = errors.New("Non 200 Response found")
)

type TwitchSubscriptionEvent struct {
	Challenge    string `json:"challenge"`
	Subscription struct {
		ID        string `json:"id"`
		Type      string `json:"type"`
		Version   string `json:"version"`
		Status    string `json:"status"`
		Cost      int    `json:"cost"`
		Condition struct {
			BroadcasterUserID string `json:"broadcaster_user_id"`
		} `json:"condition"`
		Transport struct {
			Method   string `json:"method"`
			Callback string `json:"callback"`
		} `json:"transport"`
		CreatedAt time.Time `json:"created_at"`
	} `json:"subscription"`
	Event struct {
		ID                   string    `json:"id"`
		BroadcasterUserID    string    `json:"broadcaster_user_id"`
		BroadcasterUserLogin string    `json:"broadcaster_user_login"`
		BroadcasterUserName  string    `json:"broadcaster_user_name"`
		Type                 string    `json:"type"`
		StartedAt            time.Time `json:"started_at"`
	} `json:"event"`
}

type TwitchSecretFormat struct {
	TwitchSecret string `json:"twitch-secret"`
}

func getSecret(secretId string) (string, error) {
	fmt.Println("Getting Twitch secret")
	svc := secretsmanager.New(session.New())
	input := &secretsmanager.GetSecretValueInput{
		SecretId: &secretId,
	}

	result, err := svc.GetSecretValue(input)

	if err != nil {
		if aerr, ok := err.(awserr.Error); ok {
			switch aerr.Code() {
			case secretsmanager.ErrCodeResourceNotFoundException:
				fmt.Println(secretsmanager.ErrCodeResourceNotFoundException, aerr.Error())
			case secretsmanager.ErrCodeInvalidParameterException:
				fmt.Println(secretsmanager.ErrCodeInvalidParameterException, aerr.Error())
			case secretsmanager.ErrCodeInvalidRequestException:
				fmt.Println(secretsmanager.ErrCodeInvalidRequestException, aerr.Error())
			case secretsmanager.ErrCodeDecryptionFailure:
				fmt.Println(secretsmanager.ErrCodeDecryptionFailure, aerr.Error())
			case secretsmanager.ErrCodeInternalServiceError:
				fmt.Println(secretsmanager.ErrCodeInternalServiceError, aerr.Error())
			default:
				fmt.Println(aerr.Error())
			}
		} else {
			// Print the error, cast err to awserr.Error to get the Code and
			// Message from an error.
			fmt.Println(err.Error())
		}
		return "", err
	}

	var secret TwitchSecretFormat

	err = json.Unmarshal([]byte(*result.SecretString), &secret)

	if err != nil {
		fmt.Printf("Error unmarshling secret from secrets manager: " + err.Error())
		return "", err
	}

	return secret.TwitchSecret, nil

}

func compactJson(jsonString string) (string, error) {

	bodyBytes := []byte(jsonString)

	buffer := new(bytes.Buffer)

	if err := json.Compact(buffer, bodyBytes); err != nil {
		return "", err
	}

	return buffer.String(), nil
}

func verifySignature(headers map[string]string, body string, twitchSecret string) bool {

	fmt.Println("Verifying signature")

	fmt.Printf("Raw request body: %s \n", body)

	compactBody, err := compactJson(body)

	if err != nil {
		fmt.Println("Error compacting json body: " + err.Error())
		return false
	}

	fmt.Println("Compact body: " + compactBody)

	hmacMessage := headers["Twitch-Eventsub-Message-Id"] + headers["Twitch-Eventsub-Message-Timestamp"] + compactBody

	fmt.Println("Raw Hmac Message: " + hmacMessage)

	// Create a new HMAC by defining the hash type and the key (as byte array)
	h := hmac.New(sha256.New, []byte(twitchSecret))

	// Write Data to it
	h.Write([]byte(hmacMessage))

	generatedSignature := h.Sum(nil)
	twitchSignature, err := hex.DecodeString(headers["Twitch-Eventsub-Message-Signature"][7:])

	if err != nil {
		fmt.Println("Error while decoding twitch signature: " + err.Error())
		return false
	}

	//compare hmac signatures
	if !hmac.Equal(generatedSignature, twitchSignature) {
		fmt.Printf("Generated signature %s, does not match passed in signature %s \n", hex.EncodeToString(generatedSignature), headers["Twitch-Eventsub-Message-Signature"])
		return false
	}

	return true
}

func handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {

	jsonReq, _ := json.Marshal(&request)
	fmt.Println(string(jsonReq))

	var event TwitchSubscriptionEvent

	err := json.Unmarshal([]byte(request.Body), &event)

	if err != nil {
		fmt.Printf("Error unmarshling event:")
		return events.APIGatewayProxyResponse{}, ErrNon200Response
	}

	fmt.Printf("%+v\n", event)

	//verify signature
	secret, secErr := getSecret(os.Getenv("TWITCH_SECRET_ID"))

	if secErr != nil {
		return events.APIGatewayProxyResponse{}, ErrNon200Response
	}

	if !verifySignature(request.Headers, request.Body, secret) {
		fmt.Printf("Invalid signature %s \n", request.Headers["Twitch-Eventsub-Message-Signature"])
		return events.APIGatewayProxyResponse{}, ErrNon200Response
	}

	if event.Subscription.Status == "webhook_callback_verification_pending" {

		fmt.Println("Webhook verification event")
		//respond with challenge as raw string
		return events.APIGatewayProxyResponse{
			Body:       event.Challenge,
			StatusCode: 200,
		}, nil

	} else if event.Subscription.Status == "enabled" {

		fmt.Println("Sending event to SNS")

		//TODO: send event to sns
		svc := sns.New(session.New(), aws.NewConfig().WithRegion("us-east-1"))

		msg := fmt.Sprintf("%s is live at %v", event.Event.BroadcasterUserName, event.Event.StartedAt)

		arn := os.Getenv("SNS_ARN")

		result, err := svc.Publish(&sns.PublishInput{
			Message:  &msg,
			TopicArn: &arn,
		})

		if err != nil {
			fmt.Println(err.Error())
			return events.APIGatewayProxyResponse{}, ErrNon200Response
		}

		fmt.Println(*result.MessageId)

	}

	return events.APIGatewayProxyResponse{
		StatusCode: 200,
	}, nil
}

func main() {
	lambda.Start(handler)
}
