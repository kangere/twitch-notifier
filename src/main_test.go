package main

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/hex"
	"fmt"

	"testing"
)

func TestVerifySignature(t *testing.T) {

	headers := make(map[string]string)

	headers["Twitch-Eventsub-Message-Id"] = "Id"
	headers["Twitch-Eventsub-Message-Timestamp"] = "00:00"

	data := `Id00:00{"key":"json body"}`
	secret := "secret"

	h := hmac.New(sha256.New, []byte(secret))

	// Write Data to it
	h.Write([]byte(data))

	// Get result and encode as hexadecimal string
	sha := "sha256=" + hex.EncodeToString(h.Sum(nil))

	fmt.Println(sha)

	headers["Twitch-Eventsub-Message-Signature"] = sha

	got := verifySignature(headers, `{"key":"json body"}`, secret)
	want := true

	if !got {
		t.Errorf("got: %v, wanted: %v", got, want)
	}
}
